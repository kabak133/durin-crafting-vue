import Vue from 'vue'
import Router from 'vue-router'
import home from '../views/HomePage'
import blog from '../views/BlogPage'
import singlePost from '../views/SinglePostPage'
import about from '../views/AboutUsPage'
import NotFoundComponent from '../views/NotFoundComponent'
import ContactPage from '../views/ContactPage'
import SimpleProductPage from '../views/SimpleProductPage'
import ProductsListPage from '../views/ProductsListPage'

Vue.use(Router)

export default new Router({
    mode: 'history',
    linkActiveClass: 'active',
    routes: [
        {
            path: '/',
            name: 'home',
            component: home
        },
        {
            path: '/blog',
            name: 'blog',
            component: blog,
        },
        {
            path: '/post/:slug',
            name: 'singlePost',
            component: singlePost
        },
        {
            path: '/about-us',
            name: 'about-us',
            component: about
        },
        {
            path: '/contact-us',
            name: 'contact-us',
            component: ContactPage
        },
        {
            path: '/product/:slug',
            name: 'product',
            component: SimpleProductPage
        },
        {
             path: '/products/:price?:category?:sale?',
            name: 'products',
            component:ProductsListPage
        },
        {
            path: '*',
            component: NotFoundComponent
        }
    ]
})
